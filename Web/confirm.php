<?php
    session_start();
    require_once('./db/dbhelper.php');
    // date_default_timezone_set('Asia/Ho_Chi_Minh');   
    if(!empty($_SESSION)) {
        $fullname = $_SESSION['fullname_session'];
        $gender = $_SESSION['gender_session'];
        $department = $_SESSION['department_session'];
        $date = $_SESSION['date_session'];
        $address= $_SESSION['address_session'];
        $link = $_SESSION['link_session'];
        if($department == 'Khoa học máy tính') {
            $department = 'MAT';
        }
        if($department == 'Khoa học vật liệu') {
            $department = 'KDL';
        }
        if($gender =='Nữ') {
            $gender = '1';
        }
        if($gender ==='Nam') {
            $gender = '0';
        }    
        $date_convert = str_replace('/', '-', $date);
        $new_date = date('Y-m-d', strtotime($date_convert));
        
    }
    $sql = "insert into student(name,gender, faculty, birthday, address, avartar) values('$fullname','$gender','$department','$new_date','$address','$link')";
    execute($sql);
   
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
    .infor {
        display: flex;
    }

    .label_form {
        max-width: 100px;
    }

    .form {
        line-height: 30px;
        margin-top: 8px;
    }

    .label_form {
        background-color: #499ede;
        margin-right: 20px;
        color: #fff;
        border: 2px solid #0b67ad;
    }

    .icon_red {
        color: red;
    }

    .notification {
        display: flex;
        justify-content: start;
        margin: 0;
        color: red;
        line-height: 20px;
        margin-left: 10px;
    }

    .infor_text {
        line-height: 34px;
        margin: 0;
    }
    </style>
</head>

<body>
    <center>
        <form method="post" action="complete_regis.php">
            <div style="width:40% ;" class="web">
                <div class="infor form">
                    <label class="label_form" style="flex: 1">Họ và tên <span class="icon_red">*</span></label>
                    <div>
                        <span><?=$fullname?></span>
                    </div>
                </div>
                <div class="form" style=" display: flex;">
                    <label class="label_form" style="flex: 1">Giới tính <span class="icon_red">*</span></label>
                    <div>
                        <!-- <span><?=$gender?></span> -->
                        <?php 
                             echo ($gender == '1' && isset($gender) ? '<p style="margin: 0" > Nữ </p>' : '<p style="margin: 0"> Nam </p>' );
                        ?>
                    </div>
                </div>
                <div class="form" style="display: flex;">
                    <label style="flex:1 ;" class="label_form">Phân khoa <span class="icon_red">*</span></label>
                    <div>
                        <?php echo($department == 'MAT' ? '<span>Khoa học máy tính</span>': '<span>Khoa học vật liệu</span>')  ?>
                    </div>
                </div>
                <div class="form" style=" display: flex;">
                    <label class="label_form" style="flex: 1">Ngày sinh <span class="icon_red">*</span></label>
                    <div>
                        <span><?=$date?></span>
                    </div>

                </div>
                <div class="infor form" style="display: flex;">
                    <label class="label_form label_form" style="flex: 1">Địa chỉ</label>
                    <div>
                        <span><?=$address?></span>
                    </div>

                </div>
                <div class="infor form" style="display: flex;">
                    <label class="label_form label_form" style="flex: 1; max-height: 34px;">Hình ảnh</label>
                    <div>
                        <img style="width:280px;height:200px" src="<?=$link?>" alt="ảnh uploads">
                    </div>

                </div>
                <button class="btn_submit" style=" margin-top: 20px;height: 44px; width: 120px; border-radius: 6px; border: 2px solid
                    #0b67ad; background-color: #20b835; color: #fff; font-size: 15px;">Xác
                    nhận</button>

            </div>
        </form>
    </center>

</body>


</html>