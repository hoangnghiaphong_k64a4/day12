<?php 
 require_once('config.php');
 // Su dung cho: insert, update, delete
function execute($sql) {
    $connect = mysqli_connect(HOST,USERNAME,PASSWORD,DATABASE);
    mysqli_set_charset($connect,'utf8');

   //  if($connect -> connect_error) {
   //     die("Connection failed: " . $connect->connect_error);
   //    } else {
   //       echo 'Kết nối database thành công';
   //  }
    mysqli_query($connect,$sql);
    mysqli_close($connect);
}
function executeResult($sql, $onlyOne = false) {
   $connect = mysqli_connect(HOST,USERNAME,PASSWORD,DATABASE);
    mysqli_set_charset($connect,'utf8');
    
   //  if($connect -> connect_error) {
   //    die("Connection failed: " . $connect->connect_error);
   //   } else {
   //      echo 'Kết nối database thành công';
   // }
   $result = mysqli_query($connect, $sql);
   if($onlyOne) {
      // trả về 1 sản phẩm
      $data = mysqli_fetch_array($result, 1);
   } else {
      $data =[];
      while(($row = mysqli_fetch_array($result,1)) != null) {
         $data[] = $row;
      }
   }
   mysqli_close($connect);
   return $data;
   
}